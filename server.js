//version inicial

var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

var path = require('path');

//definir el json para obtener los datos del json de mongo
var requestjson= require('request-json');

//definicion de la ruta
var urlClientesMlab = "https://api.mlab.com/api/1/databases/rbermudez/collections/Clientes?apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";
var urlCuentasMlab = "https://api.mlab.com/api/1/databases/rbermudez/collections/Cuentas?apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";
var urlMovimientosMlab = "https://api.mlab.com/api/1/databases/rbermudez/collections/Movimientos?apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";

var urlMLabRaiz = "https://api.mlab.com/api/1/databases/rbermudez/collections/";
var apiKey = "apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";
var clienteMLabRaiz;

var clienteMlab = requestjson.createClient(urlClientesMlab);

var cuentaMlab = requestjson.createClient(urlCuentasMlab);

var movimientosMlab = requestjson.createClient(urlMovimientosMlab);

var movimientosJSON = require('./movimientosv2.json');

var bodyparser = require('body-parser');
app.use (bodyparser.json());
app.use (function (req, res, next)
{
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Header", "Origin, X-Requested-With, Content-Type, Accept");
  next();
})

app.listen(port);

console.log('todo list RESTful API server started on: ' + port);

app.get ('/', function (req, res) {
 res.sendFile(path.join(__dirname,'index.html'));
})

app.post('/', function (req, res){
  res.send('Hola recibido su petición post cambiada');
})

app.put ('/', function (req, res) {
 res.send('Hola recibido su petición put');
})

app.delete('/', function (req, res){
  res.send('Hola recibido su petición delete');
})

app.get ('/Clientes/:idcliente', function (req, res) {
 res.send('Aqui tiene al cliente número ' + req.params.idcliente);
})

app.get ('/Cuentas/:idcliente', function (req, res) {
 res.send('Aqui tiene al cliente número ' + req.params.idcliente);
})

app.get ('/Movimientos/:idcuenta', function (req, res) {
 res.send('Aqui tiene al cliente número ' + req.params.idcuenta);
})

app.get ('/Movimientos', function (req, res) {
 res.sendfile('movimientosv1.json');
})
//petición con json completo
app.get ('/v2/Movimientos', function (req, res) {
 res.json(movimientosJSON);
})
//Peticion con ID especifico y JSON
app.get ('/v2/Movimientos/:id', function (req, res) {
  console.log(req.param.id);
 res.send(movimientosJSON[req.params.id-1]);
})
//Peticion con Query
app.get ('/v2/Movimientosq', function (req, res) {
  console.log(req.query);
 res.send('Query Recibido' + req.query);
})

//Peticion post para incertar datos en el bodyparser
app.post('/v2/Movimientos', function(req, res){
  var nuevo = req.body
  nuevo.id = movimientosJSON.length + 1
  movimientosJSON.push(nuevo)
  res.send('Movimiento dado de alta')
})
//Invocación para la base de datos de mongo con una
app.get ('/v1/Clientes', function (req, res){
  clienteMlab.get('', function(err, resM, body){
    if (err){
       console.log(body);
    } else {
      res.send(body);
    }
  });
});

app.post('/v1/Clientes', function (req, res){
  clienteMlab.post('', req.body, function(err, resM, body)
{
  res.send(body);
})
})

//Invocación para la base de datos de mongo con una
app.get ('/v1/Cuentas', function (req, res){
  cuentaMlab.get('', function(err, resM, body){
    if (err){
       console.log(body);
    } else {
      res.send(body);
    }
  });
});

app.post('/v1/Cuentas', function (req, res){
  cuentaMlab.post('', req.body, function(err, resM, body)
{
  res.send(body);
})
})

//Invocación para la base de datos de mongo con una
app.get ('/v1/Movimientos', function (req, res){
  movimientosMlab.get('', function(err, resM, body){
    if (err){
       console.log(body);
    } else {
      res.send(body);
    }
  });
});

app.post('/v1/Movimientos', function (req, res){
  movimientosMlab.post('', req.body, function(err, resM, body)
{
  res.send(body);
})
})


app.post('/v2/Login', function (req, res)
{
  var email = req.body.email;
  var password = req.body.password;
  var query = 'q={"email":"' + email +'", "password": "'+ password +'"}';
  console.log(query);

 clienteMLabRaiz = requestjson.createClient(urlMLabRaiz + "Usuarios?"+ apiKey + "&" + query)
 console.log (urlMLabRaiz + "Usuarios?"+ apiKey + "&" + query);
 clienteMLabRaiz.get('', function(err, resM, body)
{
  if (!err){
    if(body.length == 1){
      res.status(200).send('Usuario logado');
    }else {
    {
      res.status(404).send('Usuario Inexistente');
    }
  }
}
});
})
